#ifndef ROOMCTRL_H
#define ROOMCTRL_H

#include <QMainWindow>
#include <QWidget>
#include <QDialog>

#include <QTime>
#include <QTimer>
#include <QDateTime>

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QString>
#include <iostream>

#include <QMessageBox>

#include "event.h"
#include "port.h"

#include "cv.h"
#include "highgui.h"

using namespace std;

namespace Ui {
class RoomCTRL;
}

class RoomCTRL : public QMainWindow
{
    Q_OBJECT

public:

    explicit RoomCTRL(QWidget *parent = 0, double temperature = 0 , double humidity = 0);

    IplImage *frame;

    CvCapture *capture;

    QTimer *timer;

    Event *events;

    Port *port;

    int actual_user;

    int registros_iniciales;

    bool cam_first;

    bool cam_second;

    struct pc_usr_nodo                          // LISTA DE USUARIOS DEL PROGRAMA ROOMCTRL EN LA PC
    {
        QString pc_usr_user;
        QString pc_usr_password;
    };

    QList<RoomCTRL::pc_usr_nodo> pc_usr_list;

    struct rc_usr_nodo                          // LISTA DE USUARIOS DEL PROGRAMA ROOMCTRL EN LA INFOTRONIC
    {
        QString rc_usr_user;
        QString rc_usr_id;
    };

    QList<RoomCTRL::rc_usr_nodo> rc_usr_list;

    struct pc_reg_nodo                          // LISTA DE REGISTROS DEL PROGRAMA ROOMCTRL EN LA PC
    {
        QString pc_reg_number;
        QString pc_reg_date;
        QString pc_reg_time;
        QString pc_reg_user;
        QString pc_reg_action;
    };

    QList<RoomCTRL::pc_reg_nodo> pc_reg_list;

    struct rc_reg_nodo                          // LISTA DE REGISTROS DEL PROGRAMA ROOMCTRL EN LA INFOTRONIC
    {
        QString rc_reg_number;
        QString rc_reg_date;
        QString rc_reg_time;
        QString rc_reg_id;
        QString rc_reg_user;
        QString rc_reg_img1;
        QString rc_reg_img2;
        QString rc_reg_door;
    };

    QList<RoomCTRL::rc_reg_nodo> rc_reg_list;

    RoomCTRL * roomctrl_aux;

    ~RoomCTRL();

private slots:

    void on_B_Registers_clicked();

    void on_B_ManageUsers_clicked();

    void on_B_LogOut_clicked();

    void on_Celsius_toggled(bool checked);

    void on_Kelvin_toggled(bool checked);

    void on_Fahrenheit_toggled(bool checked);

    void Show_Time (void);

    void on_On_toggled(bool checked);

private:

    int * login_p;

    int * users_p;

    double temp;

    char unit;

    double humidity;

    bool first_img_ready;

    int actual_reg;

    Ui::RoomCTRL *ui;

    void Refresh_Temperature ( void );

    void Refresh_Humidity ( void );

    bool logout;

public slots:

    void from_login ( QList<RoomCTRL::pc_usr_nodo> , int , RoomCTRL * , Port * , int * );

    void from_users ( QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> , QList<RoomCTRL::pc_reg_nodo> );

    void Show_Camera ( );

    void from_port_tem ( QByteArray );

    void from_port_hum ( QByteArray );

    void from_port_cam ( QByteArray );

    void from_port_rfid ( QByteArray );

    void from_port_door ( QByteArray );

    void from_port_rfid_to_users ( QByteArray );

    void closeEvent ( QCloseEvent * );

signals:

    void roomctrl_to_users ( QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> , QList<RoomCTRL::pc_reg_nodo> , int , RoomCTRL * );

    void roomctrl_to_users_new_id ( QByteArray );

    void roomctrl_to_register ( QList<RoomCTRL::pc_reg_nodo> , QList<RoomCTRL::rc_reg_nodo> , QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> );

    void roomctrl_to_events_cam ( QImage * , int );

    void roomctrl_to_events_door ( );

    void roomctrl_to_events_rfid ( QString , QString );

    void validate_rfid( const QByteArray & );

};

#endif // ROOMCTRL_H
