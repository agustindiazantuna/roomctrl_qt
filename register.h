#ifndef REGISTER_H
#define REGISTER_H

#include "roomctrl.h"

namespace Ui {
class Register;
}

class Register : public QWidget
{
    Q_OBJECT

public:

    explicit Register(QWidget *parent = 0);

    QList<RoomCTRL::pc_reg_nodo> pc_reg_list_register;

    QList<RoomCTRL::rc_reg_nodo> rc_reg_list_register;

    QList<RoomCTRL::pc_usr_nodo> pc_usr_list_register;

    QList<RoomCTRL::rc_usr_nodo> rc_usr_list_register;

    ~Register();

private:

    bool close_b;

private slots:

    void on_B_Close_clicked();

    void closeEvent ( QCloseEvent * );

    void on_Room_Reg_cellClicked(int row, int column);

private:

    Ui::Register *ui;

public slots:

    void from_roomctrl(QList<RoomCTRL::pc_reg_nodo> , QList<RoomCTRL::rc_reg_nodo> , QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> );

};

#endif // REGISTER_H
