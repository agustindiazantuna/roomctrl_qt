/**
   \file port.cpp
   \brief Contiene todos los metodos de la clase Port.
   \details Administra la conexión al puerto.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "port.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Port( const QString & portName, QextSerialPort::QueryMode mode, QObject *parent )
    \brief Constructor de la clase Puerto.
    \details Configura el puerto, seteando la cantidad de bits a transmitir, si usa bit de stop, el baudrate, etc.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param portName : Tipo const QString &, nombre del puerto.
    \param mode : Tipo QextSerialPort::QueryMode, modo de conexión por eventos o polling.
    \return void
*/

Port::Port(const QString &portName, QextSerialPort::QueryMode mode, QObject *parent) :
    QObject(parent)
{
    port = new QextSerialPort(portName, mode);
    port->setBaudRate(BAUD9600);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);

    if (port->open(QIODevice::ReadWrite) == true)
    {
        bytes_full = false;

        QObject::connect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        QObject::connect(port, SIGNAL(dsrChanged(bool)), this, SLOT(onDsrChanged(bool)));

        if (!(port->lineStatus() & LS_DSR))
            qDebug() << "PORT]] warning: device is not turned on";

        emit connected(true);
    }
    else
    {
        qDebug() << "PORT]] device failed to open:" << port->errorString();

        emit connected(false);
    }
}

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Port( QObject *parent )
    \brief Constructor de la clase Puerto.
    \details Constructor por defecto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Port::Port(QObject *parent) :
    QObject(parent)
{
    port = NULL;
    bytes_full = false;
    bytes = NULL;
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Port( )
    \brief Destructor de la clase Puerto.
    \details Destruye el objeto puerto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Port::~Port(void)
{
    close();
}

// ***************************************************************************************************************************
// SLOT*CONNECT***************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void connect( const QString &portName, QextSerialPort::QueryMode mode )
    \brief Función de conexión del puerto.
    \details Configura el puerto, segun su nombre y modo.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param portName : Tipo const QString &, nombre del puerto.
    \param mode : Tipo QextSerialPort::QueryMode, modo de conexión.
    \return void
*/

void Port::connect(const QString &portName, QextSerialPort::QueryMode mode)
{
    port = new QextSerialPort(portName, mode);
    port->setBaudRate(BAUD9600);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);

    if (port->open(QIODevice::ReadWrite) == true)
    {
        bytes_full = false;

        bytes = new QByteArray;

        QObject::connect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        QObject::connect(port, SIGNAL(dsrChanged(bool)), this, SLOT(onDsrChanged(bool)));

        if (!(port->lineStatus() & LS_DSR))
            qDebug() << "PORT]] warning: device is not turned on";

        emit connected(true);
    }
    else
        qDebug() << "PORT]] device failed to open:" << port->errorString();
}

// ***************************************************************************************************************************
// SLOT*ONREADYREAD***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void onReadyRead( void )
    \brief Función de lectura del puerto.
    \details Lee el puerto, genera la trama y la emite al slot de Login onReceivedData.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Port::onReadyRead( void )
{
    int a = port->bytesAvailable();                         //Guarda en 'a', la cantidad de byte dsiponibles que tiene para leer
    QByteArray *backup = new QByteArray(*bytes);            //"Clona" el contenido de 'bytes', en backup
    QByteArray *aux = new QByteArray;                       //Auxiliar para guardar lo que lee el puerto

    aux->resize(a);                                         //Redimensiona 'aux' con la cantidad de bytes disponibles
    port->read(aux->data(), aux->size());                   //Lee el puerto y guarda en 'aux', los bytes leidos segun el tamaño de 'aux' que es 'a'
    qDebug() << "bytes read:" << aux->size();
    qDebug() << "bytes:" << QString(*aux);

    if (bytes != NULL)                                      //Si bytes contiene algo, es decir, no es la primera vez
    {
        qDebug() << "bytes had:" << QString(*bytes);        //Muestra lo que tenia bytes

        bytes->resize(backup->size() + aux->size() + 1);    //Redimensiona 'bytes' con el tamaño de 'backup' + 'aux' + 1

        *bytes = QByteArray(backup->data()) + QByteArray(aux->data());      //Guarda en 'bytes' lo que tenia 'backup' mas lo que tiene 'aux'

        qDebug() << "bytes has:" << QString(*bytes);        //Muestra lo que tiene bytes

        if (bytes->contains('#'))                           //Si 'bytes' contiene '#'
        {
            bytes_full = true;                              //La trama llego completa

            emit dataRead();                                //Emite la señal 'dataRead'
        }
    }
    else if (aux->startsWith('$'))                          //Si 'bytes' aun no tiene nada y empieza con '$'(asi empieza la trama)
    {
        qDebug() << "bytes empty. creating";                //Avisa que 'bytes' esta vacio y que se la va a crear

        bytes = new QByteArray(*aux);                       //Guarda en 'bytes', lo que contiene 'aux', que leyo del puerto

        qDebug() << "bytes has:" << QString(*bytes);        //Muestra lo que contiene 'bytes'
    }

    delete backup;                                          //Libera 'backup'
    delete aux;                                             //Libera 'aux'
}

// ***************************************************************************************************************************
// SLOT*ONDSRCHANGED**********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void onDsrChanged( bool status )
    \brief Función de estado del puerto.
    \details Sirve en modo debug para informar si el puerto está encendido o no.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param status : Tipo bool, estado del puerto.
    \return void
*/

void Port::onDsrChanged(bool status)
{
    if (status)
        qDebug() << "PORT]] device was turned on";
    else
        qDebug() << "PORT]] device was turned off";
}

// ***************************************************************************************************************************
// SLOT*SEND******************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void send( const QByteArray &cmd )
    \brief Función de escritura del puerto.
    \details Escribe en el puerto el parámetro recibido.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param cmd : Tipo const QByteArray &, comando a transmitir.
    \return void
*/

void Port::send(const QByteArray &cmd)
{
    port->write(cmd);
    qDebug() << "PORT]] sent: " << QString(cmd);
}

// ***************************************************************************************************************************
// SLOT*ISCONNECTED***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn bool isConnected( void )
    \brief Función que informa el estado del puerto.
    \details Informa si el puerto está conectado o no.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return bool : True si el puerto está conectado.
*/

bool Port::isConnected(void)
{
    if (port != NULL)
        return (port->openMode() != QIODevice::NotOpen);
    return false;
}

// ***************************************************************************************************************************
// SLOT*RECONNECT*************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void reconnect( QextSerialPort::QueryMode mode )
    \brief Función de reconexión del puerto.
    \details Cierra la conexión y la establece nuevamente.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param mode : Tipo QextSerialPort::QueryMode, modo de trabajo del puerto.
    \return void
*/

void Port::reconnect( QextSerialPort::QueryMode mode )
{
    if (port != NULL)
    {
        if (port->openMode() != QIODevice::NotOpen)
        {
            QString *portName = new QString(port->portName());

            port->close();
            connect(*portName, mode);
        }
    }
}

// ***************************************************************************************************************************
// SLOT*READ******************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn QByteArray read( void )
    \brief Función de lectura del puerto.
    \details Devuelve los bytes ya leídos que forman la trama.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return QByteArray : Trama armada.
*/

QByteArray Port::read( void )
{
    QByteArray give(*bytes);

    qDebug() << "PORT]] returning buffer" << QString(*bytes);
    bytes->clear();
    qDebug() << "PORT]] buffer clean";
    bytes_full = false;

    return give;
}

// ***************************************************************************************************************************
// SLOT*CONNECTEDTO***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void connectedTo( void )
    \brief Función de estado del puerto.
    \details Informa el nombre del puerto al que está conectado.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return QString : Nombre del puerto.
*/

QString Port::connectedTo(void)
{
    return port->portName();
}

// ***************************************************************************************************************************
// SLOT*CLOSE*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void close( void )
    \brief Función que cierra el puerto.
    \details Cierra el puerto si está abierto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Port::close(void)
{
    if (port != NULL)
        if (port->openMode() != QIODevice::NotOpen)
            emit connected(false);        

    if (bytes != NULL)
        bytes->clear();

    delete bytes;
    bytes_full = false;

    if (port != NULL)
        port->close();
}
