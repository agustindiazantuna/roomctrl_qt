/**
   \file users.cpp
   \brief Contiene todos los metodos de la clase Users.
   \details Administra los usuarios de RoomCTRL.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "users.h"
#include "ui_users.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Users(QWidget *parent)
    \brief Constructor de la clase Users.
    \details Abre la ventana de usuarios, Deshabilita los LineEdit por defecto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Users::Users(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Users)
{
    close_b = false;

    ui->setupUi(this);

    ui->LE_Password->setEchoMode(QLineEdit::Password);  // Configura el LineEdit de Password para ocultar con asteriscos el contenido.

    ui->LE_UserName->setDisabled( true );
    ui->LE_Password->setDisabled( true );
    ui->LE_ID->setDisabled( true );

    user_type = '\0';   // Por default no elige ningún tipo de usuario
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Users( )
    \brief Destructor de la clase Users.
    \details Destruye el objeto Users.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Users::~Users()
{
    on_B_Close_clicked();

    delete ui;
}

// ***************************************************************************************************************************
// HANDLER*CLOSE*EVENT********************************************************************************************************
// ***************************************************************************************************************************

/**
   \fn void closeEvent ( QCloseEvent * )
   \brief Handler del botón close de la ventana (x).
   \details Función perteneciente a la clase QWidget, llama al slot del botón logout cuando se cierra la ventana.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
   \param event : Tipo QCloseEvent *, contiene parámetros que describen el tipo de evento.
   \return void
*/

void Users::closeEvent ( QCloseEvent *event )
{
    if ( !close_b )
        on_B_Close_clicked();

    QWidget::closeEvent(event);
}

// ***************************************************************************************************************************
// RADIO*BUTTON*PC*USERS******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_PC_USERS_toggled( bool checked )
    \brief Slot de radio button PC Users toggled.
    \details Si está seleccionado el usuario PC, se habilitan los campos necesarios.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, si está seleccionado es true.
    \return void
*/

void Users::on_PC_USERS_toggled(bool checked)
{
    if ( checked )
    {
        ui->LE_UserName->setEnabled( true );
        ui->LE_Password->setEnabled( true );
        ui->LE_ID->setDisabled( true );
        ui->LE_ID->clear();

        user_type = 'P';
    }
}

// ***************************************************************************************************************************
// RADIO*BUTTON*RC*USERS******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_RC_USERS_toggled( bool checked )
    \brief Slot de radio button RC Users toggled.
    \details Si está seleccionado el usuario RC, se habilitan los campos necesarios.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, si está seleccionado es true.
    \return void
*/

void Users::on_RC_USERS_toggled(bool checked)
{
    if ( checked )
    {
        ui->LE_UserName->setEnabled( true );
        ui->LE_Password->setDisabled( true );
        ui->LE_ID->setEnabled( true );
        ui->LE_Password->clear();

        user_type = 'R';
    }
}

// ***************************************************************************************************************************
// BUTTON*CREATE**************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Create_clicked( )
    \brief Slot de button Create clicked.
    \details Dependiendo de la opción seleccionada en los radio button, se crea un usuario de PC o de RoomCTRL y se lo agrega a la lista.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Users::on_B_Create_clicked()
{
    int i;
    int autorizate = 1;

    switch ( user_type )
    {
        case 'P':   // Case User PC.
        {
            RoomCTRL::pc_usr_nodo aux1; // Nodo a agregar en la lista

            aux1.pc_usr_user = ui->LE_UserName->displayText().toLatin1();   // Se llena el nodo con los campos escritos usuario y password

            aux1.pc_usr_password = ui->LE_Password->text();

            if ( aux1.pc_usr_user == NULL || aux1.pc_usr_password == NULL ) // Si los campos de users y password están vacios, abre ventana de error.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("EMPTY SPACE");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();

                autorizate = 0;
            }

            for ( i = 0 ; i < pc_usr_list_users.size() ; i++ )  // Revisa la lista para verificar si el nombre está en uso.
            {
                if ( pc_usr_list_users[i].pc_usr_user == aux1.pc_usr_user )
                {
                    ui->Result->clear();

                    QMessageBox msgBox;
                    msgBox.setText("THAT USERNAME ALREADY EXISTS");
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.exec();

                    autorizate = 0;
                }
            }

            if ( autorizate == 1 )  // Se agrega el nodo del nuevo usuario al final de la lista.
            {
                pc_usr_list_users.insert( i , aux1 );
                ui->LE_UserName->clear();
                ui->LE_Password->clear();
                ui->Result->setText("USER CREATED");

                int j = pc_reg_list_users.size();

                QTime time = QTime::currentTime();
                QDate date = QDate::currentDate();

                RoomCTRL::pc_reg_nodo aux_reg;  // Agrega un nuevo registro a la lista de registros de la pc, la creación de un usuario.

                aux_reg.pc_reg_number = j+'0';
                aux_reg.pc_reg_date = date.toString("dd.MM.yyyy");
                aux_reg.pc_reg_time = time.toString("hh:mm:ss");
                aux_reg.pc_reg_user = pc_usr_list_users[actual_user_users].pc_usr_user;
                aux_reg.pc_reg_action = "Creo usuario (pc): "+pc_usr_list_users[i].pc_usr_user;

                pc_reg_list_users.insert( j , aux_reg );
            }
            break;
        }
        case 'R':  // Case User PC.
        {
            RoomCTRL::rc_usr_nodo aux2; // Nodo a agregar en la lista

            aux2.rc_usr_user = ui->LE_UserName->displayText().toLatin1();   // Se llena el nodo con los campos escritos usuario y password

            aux2.rc_usr_id = ui->LE_ID->displayText().toLatin1();

            if ( aux2.rc_usr_user == NULL || aux2.rc_usr_id == NULL ) // Si los campos de users e ID están vacios, abre ventana de error.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("EMPTY SPACE");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();

                autorizate = 0;
            }

            for ( i = 0 ; i < rc_usr_list_users.size() ; i++ )  // Revisa la lista para verificar si el nombre o la ID están en uso.
            {
                if ( rc_usr_list_users[i].rc_usr_user == aux2.rc_usr_user )
                {
                    ui->Result->clear();

                    QMessageBox msgBox;
                    msgBox.setText("THAT USERNAME ALREADY EXISTS");
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.exec();

                    autorizate = 0;
                }
                if ( rc_usr_list_users[i].rc_usr_id == aux2.rc_usr_id )
                {
                    ui->Result->clear();

                    QMessageBox msgBox;
                    msgBox.setText("THAT ID ALREADY EXISTS");
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.exec();

                    autorizate = 0;
                }
            }

            if ( autorizate == 1 )  // Se agrega el nodo del nuevo usuario al final de la lista.
            {
                rc_usr_list_users.insert( i , aux2 );
                ui->LE_UserName->clear();
                ui->LE_ID->clear();
                ui->Result->setText("USER CREATED");

                int j = pc_reg_list_users.size();

                QTime time = QTime::currentTime();
                QDate date = QDate::currentDate();

                RoomCTRL::pc_reg_nodo aux_reg;  // Agrega un nuevo registro a la lista de registros de la pc, la creación de un usuario.

                aux_reg.pc_reg_number = j+'0';
                aux_reg.pc_reg_date = date.toString("dd.MM.yyyy");
                aux_reg.pc_reg_time = time.toString("hh:mm:ss");
                aux_reg.pc_reg_user = pc_usr_list_users[actual_user_users].pc_usr_user;
                aux_reg.pc_reg_action = "Creo usuario (rc): "+rc_usr_list_users[i].rc_usr_user;

                pc_reg_list_users.insert( j , aux_reg );
            }
            break;
        }
        default:    // Si ningún radio button fue elegido.
        {
            QMessageBox msgBox;
            msgBox.setText("You should choose one option");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
            break;
        }
    }
}

// ***************************************************************************************************************************
// BUTTON*DELETE**************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Delete_clicked( )
    \brief Slot de button Delete clicked.
    \details Dependiendo de la opción seleccionada en los radio button, se elimina un usuario de PC o de RoomCTRL de la lista correspondiente.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Users::on_B_Delete_clicked()
{
    int i;
    int exist = 0;

    switch ( user_type )
    {
        case 'P':   // Case User PC.
        {
            RoomCTRL::pc_usr_nodo aux1;

            aux1.pc_usr_user = ui->LE_UserName->displayText().toLatin1();

            if ( aux1.pc_usr_user == NULL ) // Si el campo está vacio.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("EMPTY SPACE");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();

                exist = 1;
            }

            for ( i = 0 ; i < pc_usr_list_users.size() ; i++ )
            {
                if ( pc_usr_list_users[i].pc_usr_user == aux1.pc_usr_user )
                {
                    if ( aux1.pc_usr_user == "admin" || aux1.pc_usr_user == pc_usr_list_users[actual_user_users].pc_usr_user )
                    {
                        ui->LE_UserName->clear();
                        ui->LE_Password->clear();
                        QMessageBox msgBox;
                        msgBox.setText("YOU CAN NOT DELETE THIS USER");    // No se puede eliminar el usuario actual ni el administrador.
                        msgBox.setIcon(QMessageBox::Critical);
                        msgBox.exec();

                        exist = 1;
                    }
                    else
                    {
                        int j = pc_reg_list_users.size();   // Borra el usuario y crea el registro de esa acción.

                        QTime time = QTime::currentTime();
                        QDate date = QDate::currentDate();

                        RoomCTRL::pc_reg_nodo aux_reg;

                        aux_reg.pc_reg_number = j+'0';
                        aux_reg.pc_reg_date = date.toString("dd.MM.yyyy");
                        aux_reg.pc_reg_time = time.toString("hh:mm:ss");
                        aux_reg.pc_reg_user = pc_usr_list_users[actual_user_users].pc_usr_user;
                        aux_reg.pc_reg_action = "Borro usuario (pc): "+pc_usr_list_users[i].pc_usr_user;

                        pc_reg_list_users.insert( j , aux_reg );

                        pc_usr_list_users.removeAt( i );

                        ui->LE_UserName->clear();
                        ui->LE_Password->clear();
                        ui->Result->setText("USER DELETED");

                        exist = 1;
                    }
                }
            }

            if ( exist == 0 )   // Si el usuario no existe.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("THAT USERNAME DOES NO EXIST");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
            }
            break;
        }
        case 'R':   // Case User RoomCTRL.
        {
            RoomCTRL::rc_usr_nodo aux2;

            aux2.rc_usr_user = ui->LE_UserName->displayText().toLatin1();

            if ( aux2.rc_usr_user == NULL ) // Si un campo está vacio.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("EMPTY SPACE");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();

                exist = 1;
            }

            for ( i = 0 ; i < rc_usr_list_users.size() ; i++ )
            {
                if ( rc_usr_list_users[i].rc_usr_user == aux2.rc_usr_user ) // Si el usuario existe, lo borra y crea una entrada en el registro.
                {
                    int j = pc_reg_list_users.size();

                    QTime time = QTime::currentTime();
                    QDate date = QDate::currentDate();

                    RoomCTRL::pc_reg_nodo aux_reg;

                    aux_reg.pc_reg_number = j+'0';
                    aux_reg.pc_reg_date = date.toString("dd.MM.yyyy");
                    aux_reg.pc_reg_time = time.toString("hh:mm:ss");
                    aux_reg.pc_reg_user = pc_usr_list_users[actual_user_users].pc_usr_user;
                    aux_reg.pc_reg_action = "Borro usuario (rc): "+rc_usr_list_users[i].rc_usr_user;

                    pc_reg_list_users.insert( j , aux_reg );

                    rc_usr_list_users.removeAt( i );

                    ui->LE_UserName->clear();
                    ui->LE_ID->clear();
                    ui->Result->setText("USER DELETED");

                    exist = 1;
                }
            }

            if ( exist == 0 )   // Si no existe.
            {
                ui->Result->clear();

                QMessageBox msgBox;
                msgBox.setText("THAT USERNAME DOES NO EXIST");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
            }
            break;
        }
        default:
        {
            QMessageBox msgBox;
            msgBox.setText("You should choose one option");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
            break;
        }
    }
}

// ***************************************************************************************************************************
// BUTTON*CLOSE***************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Close_clicked( )
    \brief Slot de button Close clicked.
    \details Devuelve a la ventana RoomCTRL las listas modificadas y cierra la ventana.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Users::on_B_Close_clicked()
{
    close_b = true;

    QApplication::connect(this, SIGNAL(users_to_roomctrl ( QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> ,
                                                           QList<RoomCTRL::pc_reg_nodo> )),
                          roomctrl_aux_u, SLOT(from_users ( QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> ,
                                                           QList<RoomCTRL::pc_reg_nodo> )));

    emit users_to_roomctrl ( pc_usr_list_users , rc_usr_list_users , pc_reg_list_users );

    close();
}

// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl(QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux, QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux, QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux, int actual_user_aux , RoomCTRL * aux )
    \brief Slot de recepción de datos de la ventana RoomCTRL.
    \details Recibe datos de la ventana principal y los guarda en variables miembro.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param pc_usr_list_aux : Tipo QList<RoomCTRL::pc_usr_nodo>, lista de usuarios de la PC.
    \param rc_usr_list_aux : Tipo QList<RoomCTRL::rc_usr_nodo>, lista de usuarios de la RC.
    \param pc_reg_list_aux : Tipo QList<RoomCTRL::pc_reg_nodo>, lista de registros de la PC.
    \param actual_user_aux : Tipo int, usuario actual.
    \param aux : Tipo RoomCTRL *, puntero a la ventana RoomCTRL.
    \return void
*/

void Users::from_roomctrl(QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux ,
                          QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux , int actual_user_aux , RoomCTRL * aux )
{
    pc_usr_list_users = pc_usr_list_aux;
    rc_usr_list_users = rc_usr_list_aux;
    pc_reg_list_users = pc_reg_list_aux;
    roomctrl_aux_u = aux;
    actual_user_users = actual_user_aux;
}


// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*NEW*ID**************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl_new_id ( QByteArray id )
    \brief Slot de recepción de datos de la ventana RoomCTRL.
    \details Recibe una nueva ID.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param id : QByteArray, contiene la ID nueva.
    \return void
*/

void Users::from_roomctrl_new_id ( QByteArray id )
{
    if ( user_type == 'R' )
        ui->LE_ID->setText( id );
    else
        ui->LE_ID->clear();
}
