#ifndef EVENT_H
#define EVENT_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class Event;
}

class Event : public QWidget
{
    Q_OBJECT

public:
    explicit Event(QWidget *parent = 0);

    ~Event();

private slots:

    void on_B_Close_clicked();

private:

    Ui::Event *ui;

    QTimer *timer;

public slots:

    void event_close ( );

    void from_roomctrl_cam ( QImage * , int );

    void from_roomctrl_door ( );

    void from_roomctrl_rfid ( QString , QString );

};

#endif // EVENT_H
