#-------------------------------------------------
#
# Project created by QtCreator 2014-10-17T15:58:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(qextserialport-1.2rc/src/qextserialport.pri)

TARGET = RoomCTRL
TEMPLATE = app


SOURCES += main.cpp\
        login.cpp \
    roomctrl.cpp \
    register.cpp \
    event.cpp \
    users.cpp \
    connection.cpp \
    port.cpp

HEADERS  += login.h \
    roomctrl.h \
    register.h \
    event.h \
    users.h \
    connection.h \
    port.h

FORMS    += login.ui \
    roomctrl.ui \
    register.ui \
    event.ui \
    users.ui \
    connection.ui

CONFIG += link_pkgconfig

PKGCONFIG += opencv
