#ifndef USERS_H
#define USERS_H

#include "roomctrl.h"

namespace Ui {
class Users;
}

class Users : public QWidget
{
    Q_OBJECT

public:

    explicit Users(QWidget *parent = 0);

    int actual_user_users;

    QList<RoomCTRL::pc_usr_nodo> pc_usr_list_users;

    QList<RoomCTRL::rc_usr_nodo> rc_usr_list_users;

    QList<RoomCTRL::pc_reg_nodo> pc_reg_list_users;

    RoomCTRL * roomctrl_aux_u;

    ~Users();

private slots:

    void on_B_Create_clicked();

    void on_PC_USERS_toggled(bool checked);

    void on_RC_USERS_toggled(bool checked);

    void on_B_Delete_clicked();

    void on_B_Close_clicked();

    void closeEvent ( QCloseEvent * );

private:

    Ui::Users *ui;

    char user_type;

    RoomCTRL *roomctrl;

    bool close_b;

public slots:

    void from_roomctrl(QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> , QList<RoomCTRL::pc_reg_nodo> , int , RoomCTRL *);

    void from_roomctrl_new_id ( QByteArray );

signals:

    void users_to_roomctrl ( QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> , QList<RoomCTRL::pc_reg_nodo> );

};

#endif // USERS_H
