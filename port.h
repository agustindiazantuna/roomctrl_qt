#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <iostream>

#include <qextserialport.h>
#include <qextserialenumerator.h>

using namespace std;

class Port : public QObject
{
    Q_OBJECT

public:

    explicit Port(const QString &portName,  QextSerialPort::QueryMode mode = QextSerialPort::EventDriven, QObject *parent = 0);

    explicit Port(QObject *parent = 0);

    ~Port(void);

    QByteArray read(void);

    void close(void);

    bool isConnected(void);

    void connect(const QString &portName,  QextSerialPort::QueryMode mode = QextSerialPort::EventDriven);

    void reconnect(QextSerialPort::QueryMode mode = QextSerialPort::EventDriven);

    QString connectedTo(void);

signals:

    void dataRead();

    void connected(bool);

public slots:

    void send(const QByteArray &);

private slots:

    void onReadyRead(void);

    void onDsrChanged(bool status);

private:

    QextSerialPort *port;

    QByteArray *bytes;

    bool bytes_full;
};

#endif // PORT_H
