/**
   \file event.cpp
   \brief Contiene todos los metodos de la clase Event.
   \details Muestra las fotos tomadas, los usuarios que intentan acceder con su ID, y el estado de la puerta.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "event.h"
#include "ui_event.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Event(QWidget *parent)
    \brief Constructor de la clase Event.
    \details Abre la ventana de eventos, dispara un timer de 45 segundos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Event::Event(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Event)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(event_close()));
    timer->start(35000);
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Event()
    \brief Destructor de la clase Event.
    \details Elimina la ventana de eventos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Event::~Event()
{
    delete ui;
}

// ***************************************************************************************************************************
// BUTTON*CLOSE***************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Close_clicked(void)
    \brief Slot de clickeo del boton close.
    \details Cierra la ventana de eventos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Event::on_B_Close_clicked()
{
    close();
}

// ***************************************************************************************************************************
// METODO*EVENT_CLOSE*********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void event_close(void)
    \brief Función que cierra la ventana.
    \details Cierra la ventana de eventos, se activa cuando llega la señal timeout disparada en el constructor luego de 45 segundos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Event::event_close ( )
{
    close();
}

// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*CAM*****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl_cam(QImage * qimg , int num_cam)
    \brief Slot de comunicación con la ventana Roomctrl.
    \details Recibe la foto tomada y el numero de foto del evento (primera o segunda foto).
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param qimg : Puntero tipo QImage a la imagen tomada por la cámara.
    \param num_cam : Tipo int, número de imagen.
    \return void
*/

void Event::from_roomctrl_cam ( QImage * qimg , int num_cam )
{
    QImage *scaled_image = new QImage(qimg->scaled(350,350,Qt::KeepAspectRatio));

    if ( num_cam == 1 )
        ui->Cam_View_1->setPixmap(QPixmap::fromImage(scaled_image->mirrored(true,false)));

    if ( num_cam == 2 )
        ui->Cam_View_2->setPixmap(QPixmap::fromImage(scaled_image->mirrored(true,false)));
}

// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*DOOR****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl_door(void)
    \brief Slot de comunicación con la ventana Roomctrl.
    \details Si se emite una señal hacia este slot, en el label State se muestra "Door Open".
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Event::from_roomctrl_door ( )
{
    ui->State->setText("Door Open");
    timer->start(10000);
}

// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*RFID****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl_rfid(QString id , QString user)
    \brief Slot de comunicacion con la ventana Roomctrl.
    \details Recibe dos QString correspondientes a la ID y al usuario, y los muestra en pantalla.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param id : Tipo QString, número de ID.
    \param user : Tipo QString, nombre de usuario.
    \return void
*/

void Event::from_roomctrl_rfid ( QString id , QString user )
{
    ui->UID->setText(id);
    ui->Uname->setText(user);

    if ( user != "ACCESS DENIED" )
        timer->start(45000);
    else
        timer->start(5000);
}
