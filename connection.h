#ifndef CONNECTION_H
#define CONNECTION_H

#include <QWidget>
#include <QDialog>
#include <QMessageBox>
#include <qextserialport.h>
#include <qextserialenumerator.h>

using namespace std;

namespace Ui {
class Connection;
}

class Connection : public QDialog
{
    Q_OBJECT

public:

    explicit Connection(bool state = false, QWidget *parent = 0);

    ~Connection();

    void EnumeratePorts(void);

private slots:

    void on_B_Refresh_clicked();

    void on_B_Connect_clicked();

private:

    Ui::Connection *ui;

public slots:

    void UpdateConnectionState(bool connected);

signals:

    void connectClicked(QString portName);
};

#endif // CONNECTION_H
