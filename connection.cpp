/**
   \file connection.cpp
   \brief Contiene todos los metodos de la clase Connection.
   \details Busqueda de los puertos disponibles, selección y conexión.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "connection.h"
#include "ui_connection.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Connection( bool state, QWidget *parent )
    \brief Constructor de la clase Connection.
    \details Abre la ventana de conexion, enumera los puertos disponibles y actualiza el estado de conexion.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param state: Es tipo bool, true conectado, false desconectado.
    \return void
*/

Connection::Connection(bool state, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Connection)
{
    ui->setupUi(this);

    EnumeratePorts();
    UpdateConnectionState(state);
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Connection()
    \brief Destructor de la clase Connection.
    \details Elimina la ventana.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Connection::~Connection()
{
    delete ui;
}

// ***************************************************************************************************************************
// BUTTON*REFRESH*************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Refresh_clicked(void)
    \brief Slot de clickeo del boton refresh.
    \details Llama a la funcion EnumeratePorts().
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Connection::on_B_Refresh_clicked()
{
   EnumeratePorts();
}

// ***************************************************************************************************************************
// BUTTON*CONNECT*************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Connect_clicked(void)
    \brief Slot de clickeo del boton connect.
    \details Conecta el puerto seleccionado en la ComboBox.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Connection::on_B_Connect_clicked()
{
    QString portName;
    int i = ui->ComboPort->currentIndex();

    if (i != -1 && ui->ComboPort->itemText(i) == ui->ComboPort->currentText())
        portName = ui->ComboPort->itemData(i).toString();
    else
        portName = ui->ComboPort->currentText();

    emit connectClicked(portName);
}

// ***************************************************************************************************************************
// METODO*ENUMERATEPORTS******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void EnumeratePorts(void)
    \brief Función que enumera los puertos disponibles.
    \details Genera una lista con los puertos disponibles y los carga en una ComboBox.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Connection::EnumeratePorts()
{
    ui->ComboPort->clear();

    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

    foreach (const QextPortInfo port, ports)
    {
#ifdef Q_OS_WIN
        ui->ComboPort->addItem(port.friendName, port.portName);
#elif (defined Q_OS_UNIX)
        ui->ComboPort->addItem(port.friendName, port.physName);
#endif
    }
}

// ***************************************************************************************************************************
// METODO*UPDATECONNECTIONSTATE***********************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void UpdateConnectionState(bool connected)
    \brief Función que actualiza el estado de la ventana.
    \details Actualiza el estado de conexion (connected, disconnected) y habilita el boton connect.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param connected: Es tipo bool, true habilitar botón, false deshabilitar botón.
    \return void
*/

void Connection::UpdateConnectionState(bool connected)
{
    if (connected)
    {
        ui->State->setStyleSheet("font-weight: bold; color: black; background-color: lightgreen;");
        ui->State->setText("Connected");
        ui->B_Connect->setText("Disconnect");
        ui->B_Connect->setEnabled(true);
    }
    else
    {
        ui->State->setStyleSheet("font-weight: normal; color: white; background-color: red;");
        ui->State->setText("Disconnected");
        ui->B_Connect->setText("Connect");
        ui->B_Connect->setEnabled(true);
    }
}
