/**
   \file roomctrl.cpp
   \brief Contiene todos los metodos de la clase RoomCTRL.
   \details Ventana principal del programa.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "roomctrl.h"
#include "ui_roomctrl.h"

#include "login.h"
#include "register.h"
#include "users.h"


#include <QApplication>
#include "qapplication.h"

// ***************************************************************************************************************************
// CONTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn RoomCTRL( QWidget *parent, double temperature, double humidity )
    \brief Constructor de la clase RoomCTRL.
    \details Configura los valores iniciales de la ventana, carga las listas y muestra los registros en la QTable.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param temperature : Tipo double, valor inicial de la temperatura.
    \param humidity : Tipo double, valor inicial de la humedad.
    \return void
*/

RoomCTRL::RoomCTRL(QWidget *parent, double temperature, double humidity) :
    QMainWindow(parent), capture(NULL), temp(temperature), unit('C'), humidity(humidity),
    ui(new Ui::RoomCTRL)
{
    ui->setupUi(this);
    ui->LCDhum->display( humidity = 0 );
    ui->Off->toggle();
    ui->Celsius->toggle();
    Refresh_Temperature();
    Refresh_Humidity();

    first_img_ready = false;

    logout = false;

    ui->Registers->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(Show_Time()));
    timer->start();

    QDir directory(".");                                                // Abrimos pc_usr
    QString path = directory.filePath("reg/rc_usr.txt");
    QFile file_rc_usr ( path );

    if( file_rc_usr.open(QIODevice::ReadOnly) )                         // Si existe lleno lista
    {
        while ( !file_rc_usr.atEnd() )
        {
            QString data = file_rc_usr.readLine(300);
            data.remove( QRegExp("\n") );
            QStringList qsl_aux = data.split(",");

            rc_usr_nodo aux;
            aux.rc_usr_user = qsl_aux[0];
            aux.rc_usr_id = qsl_aux[1];

            rc_usr_list.append(aux);
        }
        file_rc_usr.close();
    }

    path = directory.filePath("reg/pc_reg.txt");
    QFile file_pc_reg ( path );

    if( file_pc_reg.open(QIODevice::ReadOnly) )                         // Si existe lleno lista
    {
        while ( !file_pc_reg.atEnd() )
        {
            QString data = file_pc_reg.readLine(300);
            data.remove( QRegExp("\n") );
            QStringList qsl_aux = data.split(",");

            pc_reg_nodo aux;
            aux.pc_reg_number = qsl_aux[0];
            aux.pc_reg_date = qsl_aux[1];
            aux.pc_reg_time = qsl_aux[2];
            aux.pc_reg_user = qsl_aux[3];
            aux.pc_reg_action = qsl_aux[4];

            pc_reg_list.append(aux);
        }
        file_pc_reg.close();
    }

    path = directory.filePath("reg/rc_reg.txt");
    QFile file_rc_reg ( path );

    if( file_rc_reg.open(QIODevice::ReadOnly) )                       // Si existe lleno lista
    {
        while ( !file_rc_reg.atEnd() )
        {
            QString data = file_rc_reg.readLine(300);
            data.remove( QRegExp("\n") );
            QStringList qsl_aux = data.split(",");

            rc_reg_nodo aux;
            aux.rc_reg_number = qsl_aux[0];
            aux.rc_reg_date = qsl_aux[1];
            aux.rc_reg_time = qsl_aux[2];
            aux.rc_reg_id = qsl_aux[3];
            aux.rc_reg_user = qsl_aux[4];
            aux.rc_reg_img1 = qsl_aux[5];
            aux.rc_reg_img2 = qsl_aux[6];
            aux.rc_reg_door = qsl_aux[7];

            rc_reg_list.append(aux);
        }
        file_rc_reg.close();
    }

    registros_iniciales = rc_reg_list.size();

    ui->Registers->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);            /// A PARTIR DE Qt V5.0
    ui->Registers->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->Registers->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->Registers->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->Registers->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~RoomCTRL( )
    \brief Destructor de la clase RoomCTRL.
    \details Destruye el objeto RoomCTRL.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

RoomCTRL::~RoomCTRL()
{
    delete ui;
}

// ***************************************************************************************************************************
// HANDLER*CLOSE*EVENT********************************************************************************************************
// ***************************************************************************************************************************

/**
   \fn void closeEvent ( QCloseEvent *event )
   \brief Handler del botón close de la ventana (x).
   \details Función perteneciente a la clase MainWindow, llama al slot del botón logout cuando se cierra la ventana.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
   \param event : Tipo QCloseEvent *, contiene parámetros que describen el tipo de evento.
   \return void
*/

void RoomCTRL::closeEvent ( QCloseEvent *event )
{
    if ( !logout )
        on_B_LogOut_clicked();

    QMainWindow::closeEvent(event);
}

// ***************************************************************************************************************************
// BUTTON*REGISTERS***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Registers_clicked( )
    \brief Slot del botón registers al ser clickeado.
    \details Crea la ventana de Registros, realiza la conexión con signals y slots y emite la señal correspondiente.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::on_B_Registers_clicked()
{
    Register *registers;
    registers = new Register;
    registers->show();

    QApplication::connect(this, SIGNAL(roomctrl_to_register ( QList<RoomCTRL::pc_reg_nodo> , QList<RoomCTRL::rc_reg_nodo> ,
                                                              QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> )) ,
                          registers, SLOT(from_roomctrl(QList<RoomCTRL::pc_reg_nodo> , QList<RoomCTRL::rc_reg_nodo>,
                                                        QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> )));

    emit roomctrl_to_register ( pc_reg_list , rc_reg_list , pc_usr_list , rc_usr_list );
}

// ***************************************************************************************************************************
// BUTTON*MANAGE*USERS********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_ManageUsers_clicked( )
    \brief Slot del botón manage users al ser clickeado.
    \details Crea la ventana de Users, realiza la conexión con signals y slots y emite la señal correspondiente.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::on_B_ManageUsers_clicked( )
{
    Users *users;
    users = new Users;
    users->show();
    users_p = (int *) users;

    QApplication::connect(this, SIGNAL(roomctrl_to_users(QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> ,
                                                         QList<RoomCTRL::pc_reg_nodo> , int , RoomCTRL *)),
                          users, SLOT(from_roomctrl(QList<RoomCTRL::pc_usr_nodo> , QList<RoomCTRL::rc_usr_nodo> ,
                                                    QList<RoomCTRL::pc_reg_nodo> , int , RoomCTRL *)));

    emit roomctrl_to_users ( pc_usr_list , rc_usr_list , pc_reg_list , actual_user , roomctrl_aux );
}

// ***************************************************************************************************************************
// BUTTON*LOGOUT**************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_LogOut_clicked( )
    \brief Slot del botón logout al ser clickeado.
    \details Guarda las listas en los archivos, libera recursos de la cámara, cierra la ventana actual y abre la de Login.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::on_B_LogOut_clicked()
{
    logout = true;

    QDir directory(".");
    QString path = directory.filePath("reg/pc_usr.txt");
    QFile file_pc_usr (path);

    if( file_pc_usr.open(QIODevice::WriteOnly))
    {
        QTextStream myfile(&file_pc_usr);
        for( int i = 0 ; i < pc_usr_list.size() ; i++ )
        {
            myfile << pc_usr_list[i].pc_usr_user.toLocal8Bit().constData() << "," <<
                      pc_usr_list[i].pc_usr_password.toLocal8Bit().constData() << endl;
        }
        file_pc_usr.close();
    }

    path = directory.filePath("reg/rc_usr.txt");
    QFile file_rc_usr (path);

    if( file_rc_usr.open(QIODevice::WriteOnly))
    {
        QTextStream myfile(&file_rc_usr);
        for( int i = 0 ; i < rc_usr_list.size() ; i++ )
        {
            myfile << rc_usr_list[i].rc_usr_user.toLocal8Bit().constData() << "," <<
                      rc_usr_list[i].rc_usr_id.toLocal8Bit().constData() << endl;
        }
        file_rc_usr.close();
    }

    path = directory.filePath("reg/pc_reg.txt");
    QFile file_pc_reg (path);

    if( file_pc_reg.open(QIODevice::WriteOnly))
    {
        QTextStream myfile(&file_pc_reg);
        for( int i = 0 ; i < pc_reg_list.size() ; i++ )
        {
            myfile << pc_reg_list[i].pc_reg_number.toLocal8Bit().constData() << "," <<
                      pc_reg_list[i].pc_reg_date.toLocal8Bit().constData() << "," <<
                      pc_reg_list[i].pc_reg_time.toLocal8Bit().constData() << "," <<
                      pc_reg_list[i].pc_reg_user.toLocal8Bit().constData() << "," <<
                      pc_reg_list[i].pc_reg_action.toLocal8Bit().constData() << endl;
        }
        file_pc_reg.close();
    }

    path = directory.filePath("reg/rc_reg.txt");
    QFile file_rc_reg (path);

    if( file_rc_reg.open(QIODevice::WriteOnly))
    {
        QTextStream myfile(&file_rc_reg);
        for( int i = 0 ; i < rc_reg_list.size() ; i++ )
        {
            myfile << rc_reg_list[i].rc_reg_number.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_date.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_time.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_id.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_user.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_img1.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_img2.toLocal8Bit().constData() << "," <<
                      rc_reg_list[i].rc_reg_door.toLocal8Bit().constData() << endl;
        }
        file_rc_reg.close();
    }

    if ( capture != NULL )
    {
        cvReleaseCapture(&capture);
        timer->stop();
    }

    Login *login;
    login = ( Login * )login_p ;
    login->show();

    login->user_logged = false;

    close();
}

// ***************************************************************************************************************************
// RADIO*BUTTON*CELSIUS*******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_Celsius_toggled( bool checked )
    \brief Slot del radio botón Celsius al estar pulsado.
    \details Modifica la variable miembro de unidad y refresca la temperatura en pantalla.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, true si está pulsado.
    \return void
*/

void RoomCTRL::on_Celsius_toggled(bool checked)
{
    if( checked )
        unit = 'C';
    Refresh_Temperature();
}

// ***************************************************************************************************************************
// RADIO*BUTTON*KELVIN********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_Kelvin_toggled( bool checked )
    \brief Slot del radio botón Kelvin al estar pulsado.
    \details Modifica la variable miembro de unidad y refresca la temperatura en pantalla.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, true si está pulsado.
    \return void
*/

void RoomCTRL::on_Kelvin_toggled(bool checked)
{
   if( checked )
       unit = 'K';
   Refresh_Temperature();
}

// ***************************************************************************************************************************
// RADIO*BUTTON*FAHRENHEIT****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_Fahrenheit_toggled( bool checked )
    \brief Slot del radio botón Fharenheit al estar pulsado.
    \details Modifica la variable miembro de unidad y refresca la temperatura en pantalla.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, true si está pulsado.
    \return void
*/

void RoomCTRL::on_Fahrenheit_toggled(bool checked)
{
    if( checked )
        unit = 'F';
    Refresh_Temperature();
}

// ***************************************************************************************************************************
// RADIO*BUTTON*ON*CAMERA*****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_On_toggled( bool checked )
    \brief Slot del radio botón ON al estar pulsado.
    \details Si está pulsado abre la cámara y dispara el timer de refresco del slot de la imagen, en caso contrario libera recursos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param checked : Tipo bool, true si está pulsado.
    \return void
*/

void RoomCTRL::on_On_toggled(bool checked)
{
    if ( checked )
    {
        capture = cvCaptureFromCAM(0);

        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(Show_Camera( )));
        timer->start(35);
    }
    else
    {
        timer->stop();
        ui->L_Camera->clear();
        ui->L_Camera->setText("\t\t\t\t\tCAMERA OFF");
        cvReleaseCapture(&capture);
        capture = NULL;
    }
}

// ***************************************************************************************************************************
// SLOT*REFRESH*HUMIDITY******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void Refresh_Humidity( void )
    \brief Slot de refresco del display de Humedad.
    \details Carga en el display el valor de la variable miembro humedad.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::Refresh_Humidity ( void )
{
    ui->LCDhum->display( humidity );
}

// ***************************************************************************************************************************
// SLOT*REFRESH*TEMPERATURE***************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void Refresh_Temperature( void )
    \brief Slot de refresco del display de Temperatura.
    \details En base a la unidad actual refresca el valor en el display.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::Refresh_Temperature ( void )
{
    double temperature = 0;
    switch( unit )
    {
        case 'C':
            temperature = temp;
            break;
        case 'K':
            temperature = temp + 273.15;
            break;
        case 'F':
            temperature = (temp * 9.0)/5 + 32;
            break;
        default:
            temperature = temp;
            break;
    }

    ui->LCDtemp->display( temperature );
}

// ***************************************************************************************************************************
// SLOT*SHOW*TIME*************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void Show_Time( void )
    \brief Slot de refresco del reloj.
    \details En base a la hora actual refresca el reloj.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::Show_Time( void )
{
    QTime time = QTime::currentTime();
    QString time_text = time.toString("hh:mm:ss");
    ui->Digital_Clock->setText(time_text);
}

// ***************************************************************************************************************************
// SLOT*SHOW*CAMERA***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void Show_Camera( void )
    \brief Slot de refresco de la cámara.
    \details Toma un frame, convierte la imagen de IplImage a QImage y lo muestra en un label.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void RoomCTRL::Show_Camera ( )
{
    if(!capture)
        ui->L_Camera->setText("CAPTURE IS NULL");

    frame = cvQueryFrame(capture);			//Se obtiene cada frame
    if(!frame)
        ui->L_Camera->setText("FRAME IS NULL");

//  FUENTE = http://umanga.wordpress.com/2010/04/19/how-to-covert-qt-qimage-into-opencv-iplimage-and-viceversa/

    int h = frame->height;
    int w = frame->width;
    int channels = frame->nChannels;
    QImage *qimg = new QImage(w, h, QImage::Format_ARGB32);
    char *data = frame->imageData;

    for (int y = 0; y < h; y++, data += frame->widthStep)
    {
        for (int x = 0; x < w; x++)
        {
            char r = 0, g = 0, b = 0, a = 0;
            if (channels == 1)
            {
                r = data[x * channels];
                g = data[x * channels];
                b = data[x * channels];
            }
            else if (channels == 3 || channels == 4)
            {
                r = data[x * channels + 2];
                g = data[x * channels + 1];
                b = data[x * channels];
            }

            if (channels == 4)
            {
                a = data[x * channels + 3];
                qimg->setPixel(x, y, qRgba(r, g, b, a));
            }
            else           
                qimg->setPixel(x, y, qRgb(r, g, b));           
        }
    }

    ui->L_Camera->setPixmap(QPixmap::fromImage(qimg->mirrored(true,false)));    // Modo espejo con true y false ( horizontal , vertical )

    delete qimg;
}

// ***************************************************************************************************************************
// SLOT*FROM*LOGIN************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_login( QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , int i, RoomCTRL * aux, Port * port_aux, int * login_aux )
    \brief Slot de recepción de la ventana Login.
    \details Recibe la lista de usuarios de la PC, el número de usuario actual, el puntero a RoomCTRL y al puerto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param pc_usr_list_aux : Tipo QList<RoomCTRL::pc_usr_nodo>, lista de usuarios de la PC.
    \param i : Tipo int, usuario actual.
    \param aux : Tipo RoomCTRL *, puntero a la ventana actual.
    \param port_aux : Tipo Port *, puntero al puerto.
    \param login_aux : Tipo int *, puntero backup a la ventana de login.
    \return void
*/

void RoomCTRL::from_login ( QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , int i , RoomCTRL * aux , Port * port_aux , int * login_aux )
{
    actual_user = i;

    login_p = login_aux;
    port = port_aux;
    roomctrl_aux = aux;
    pc_usr_list = pc_usr_list_aux;

    ui->Uname->setText(pc_usr_list[actual_user].pc_usr_user);
}

// ***************************************************************************************************************************
// SLOT*FROM*USERS************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_users( QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux, QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux, QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux )
    \brief Slot de recepción de la ventana Users.
    \details Recibe la lista de usuarios de la PC, de RoomCTRL y la lista de registros de la PC.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param pc_usr_list_aux : Tipo QList<RoomCTRL::pc_usr_nodo>, lista de usuarios de la PC.
    \param rc_usr_list_aux : Tipo QList<RoomCTRL::rc_usr_nodo>, lista de usuarios de la placa.
    \param pc_reg_list_aux : QList<RoomCTRL::pc_reg_nodo>, lista de registros de la PC.
    \return void
*/

void RoomCTRL::from_users ( QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux,
                            QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux )
{
    pc_usr_list = pc_usr_list_aux;
    rc_usr_list = rc_usr_list_aux;
    pc_reg_list = pc_reg_list_aux;
}

// ***************************************************************************************************************************
// SLOT*FROM*PORT*HUM*********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_port_hum( QByteArray aux )
    \brief Slot de recepción de la humedad del puerto.
    \details Recibe la humedad y llama a Refresh_Humidity().
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param aux : Tipo QByteArray, valor de humedad recibido de la placa.
    \return void
*/

void RoomCTRL::from_port_hum ( QByteArray aux )
{
    humidity = aux.toDouble();
    Refresh_Humidity();
}

// ***************************************************************************************************************************
// SLOT*FROM*PORT*TEMP********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_port_tem( QByteArray aux )
    \brief Slot de recepción de la temperatura del puerto.
    \details Recibe la temperatura y llama a Refresh_Temperature().
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param aux : Tipo QByteArray, valor de temperatura recibido de la placa.
    \return void
*/

void RoomCTRL::from_port_tem ( QByteArray aux )
{
    temp = aux.toDouble();
    Refresh_Temperature();
}

// ***************************************************************************************************************************
// SLOT*FROM*PORT*CAM*********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_port_cam( QByteArray aux )
    \brief Slot de recepción de la cámara del puerto.
    \details Recibe número de foto, captura la imagen, levanta la ventana de Eventos, guarda la imagen y genera una entrada en los registros de la placa.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param aux : Tipo QByteArray, valor de la cámara recibido de la placa.
    \return void
*/

void RoomCTRL::from_port_cam ( QByteArray aux )
{
    bool cam_was_active = true;

    if ( aux.toDouble() == 1 && cam_first == false )
    {
        cam_first = true;

        IplImage * frame_aux;

        events = new Event;
        events->show();

        if( capture == NULL )
        {
            cam_was_active = false;
            capture = cvCaptureFromCAM(0);

            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
        }

        frame_aux = frame;

        int h = frame_aux->height;
        int w = frame_aux->width;
        int channels = frame_aux->nChannels;
        QImage *qimg = new QImage(w, h, QImage::Format_ARGB32);
        char *data = frame_aux->imageData;

        for (int y = 0; y < h; y++, data += frame_aux->widthStep)
        {
            for (int x = 0; x < w; x++)
            {
                char r = 0, g = 0, b = 0, a = 0;
                if (channels == 1)
                {
                    r = data[x * channels];
                    g = data[x * channels];
                    b = data[x * channels];
                }
                else if (channels == 3 || channels == 4)
                {
                    r = data[x * channels + 2];
                    g = data[x * channels + 1];
                    b = data[x * channels];
                }

                if (channels == 4)
                {
                    a = data[x * channels + 3];
                    qimg->setPixel(x, y, qRgba(r, g, b, a));
                }
                else               
                    qimg->setPixel(x, y, qRgb(r, g, b));              
            }
        }

        QApplication::connect(this, SIGNAL(roomctrl_to_events_cam( QImage * , int )),
                              events, SLOT(from_roomctrl_cam( QImage * , int )));

        emit roomctrl_to_events_cam ( qimg , aux.toInt() );

        QApplication::disconnect(this, SIGNAL(roomctrl_to_events_cam( QImage * , int )),
                              events, SLOT(from_roomctrl_cam( QImage * , int )));

        if ( cam_was_active == false )
        {
            cvReleaseCapture(&capture);
            capture = NULL;
        }

        int j = rc_reg_list.size();

        actual_reg = j;

        QTime time = QTime::currentTime();
        QDate date = QDate::currentDate();

        RoomCTRL::rc_reg_nodo aux_reg;

        QString num = 0;
        num[0] = j%10 + '0';
        if ( j/10 > 0 )
        {
            num[1] = j%10 + '0';
            j = j/10;
            num[0] = j%10 + '0';
        }

        aux_reg.rc_reg_number = num;
        aux_reg.rc_reg_date = date.toString("dd.MM.yyyy");
        aux_reg.rc_reg_time = time.toString("hh:mm:ss");

        qimg->save(QDir::currentPath()+"/img/"+aux_reg.rc_reg_number.toLatin1()+"_img_1.jpg");

        aux_reg.rc_reg_img1 = aux_reg.rc_reg_number.toLatin1()+"_img_1.jpg";

        rc_reg_list.insert( actual_reg , aux_reg );

        first_img_ready = true;

        delete qimg;

        while ( ui->Registers->rowCount() > 0)
            ui->Registers->removeRow(0);

        for( int i = actual_reg ; i > registros_iniciales -1 ; i-- )
        {
            ui->Registers->insertRow(ui->Registers->rowCount());

            ui->Registers->setItem(ui->Registers->rowCount()-1, 0, new QTableWidgetItem(rc_reg_list[i].rc_reg_date));
            ui->Registers->setItem(ui->Registers->rowCount()-1, 1, new QTableWidgetItem(rc_reg_list[i].rc_reg_time));
            ui->Registers->setItem(ui->Registers->rowCount()-1, 2, new QTableWidgetItem(rc_reg_list[i].rc_reg_id));
            ui->Registers->setItem(ui->Registers->rowCount()-1, 3, new QTableWidgetItem(rc_reg_list[i].rc_reg_user));
            ui->Registers->setItem(ui->Registers->rowCount()-1, 4, new QTableWidgetItem(rc_reg_list[i].rc_reg_door));
        }

    }
    else if ( aux.toDouble() == 2 && first_img_ready == true && cam_second == false )
    {
        IplImage * frame_aux;

        cam_second = true;

        if( capture == NULL )
        {
            cam_was_active = false;
            capture = cvCaptureFromCAM(0);

            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
            frame = cvQueryFrame(capture);			//Se obtiene cada frame
        }

        frame_aux = frame;

        int h = frame_aux->height;
        int w = frame_aux->width;
        int channels = frame_aux->nChannels;
        QImage *qimg = new QImage(w, h, QImage::Format_ARGB32);
        char *data = frame_aux->imageData;

        for (int y = 0; y < h; y++, data += frame_aux->widthStep)
        {
            for (int x = 0; x < w; x++)
            {
                char r = 0, g = 0, b = 0, a = 0;
                if (channels == 1)
                {
                    r = data[x * channels];
                    g = data[x * channels];
                    b = data[x * channels];
                }
                else if (channels == 3 || channels == 4)
                {
                    r = data[x * channels + 2];
                    g = data[x * channels + 1];
                    b = data[x * channels];
                }

                if (channels == 4)
                {
                    a = data[x * channels + 3];
                    qimg->setPixel(x, y, qRgba(r, g, b, a));
                }
                else                
                    qimg->setPixel(x, y, qRgb(r, g, b));                
            }
        }

        QApplication::connect(this, SIGNAL(roomctrl_to_events_cam( QImage * , int )),
                              events, SLOT(from_roomctrl_cam( QImage * , int )));

        emit roomctrl_to_events_cam ( qimg , aux.toInt( ) );

        QApplication::disconnect(this, SIGNAL(roomctrl_to_events_cam( QImage * , int )),
                              events, SLOT(from_roomctrl_cam( QImage * , int )));

        if ( cam_was_active == false )
        {
            cvReleaseCapture(&capture);
            capture = NULL;
        }

        qimg->save(QDir::currentPath()+"/img/"+rc_reg_list[actual_reg].rc_reg_number.toLatin1()+"_img_2.jpg");

        rc_reg_list[actual_reg].rc_reg_img2 = rc_reg_list[actual_reg].rc_reg_number.toLatin1()+"_img_2.jpg";

        first_img_ready = false;

        delete qimg;
    }
}

// ***************************************************************************************************************************
// SLOT*FROM*PORT*DOOR********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_port_door( QByteArray aux )
    \brief Slot de recepción del estado de la puerta.
    \details Recibe un 1 cuando la puerta se abrió, modifica el registro actual y envía la string a la ventana de Eventos.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param aux : Tipo QByteArray, valor de la puerta recibido de la placa.
    \return void
*/

void RoomCTRL::from_port_door ( QByteArray aux )
{
    if ( aux.toDouble() == 1 )
        rc_reg_list[actual_reg].rc_reg_door = "Door Open";

    QApplication::connect(this, SIGNAL(roomctrl_to_events_door( )), events, SLOT(from_roomctrl_door( )));

    emit roomctrl_to_events_door ( );

    QApplication::disconnect(this, SIGNAL(roomctrl_to_events_door( )), events, SLOT(from_roomctrl_door( )));

    while ( ui->Registers->rowCount() > 0)
        ui->Registers->removeRow(0);

    for( int j = actual_reg ; j > registros_iniciales -1 ; j-- )
    {
        ui->Registers->insertRow(ui->Registers->rowCount());

        ui->Registers->setItem(ui->Registers->rowCount()-1, 0, new QTableWidgetItem(rc_reg_list[j].rc_reg_date));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 1, new QTableWidgetItem(rc_reg_list[j].rc_reg_time));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 2, new QTableWidgetItem(rc_reg_list[j].rc_reg_id));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 3, new QTableWidgetItem(rc_reg_list[j].rc_reg_user));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 4, new QTableWidgetItem(rc_reg_list[j].rc_reg_door));
    }
}

 // ***************************************************************************************************************************
 // SLOT*FROM*PORT*RFID********************************************************************************************************
 // ***************************************************************************************************************************

/**
    \fn void from_port_rfid( QByteArray aux )
    \brief Slot de recepción del rfid.
    \details Recibe una string con la id de la tarjeta leída. Comprueba si es un usuario válido y devuelve un 1 si lo es a través del puerto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param aux : Tipo QByteArray, valor de id recibido de la placa.
    \return void
*/

void RoomCTRL::from_port_rfid ( QByteArray id )
{
    int autorizate = 0;

    for ( int i = 0 ; i < rc_usr_list.size() ; i++ )              // Comprueba si user-id están en la lista
    {
        if ( rc_usr_list[i].rc_usr_id == id )
        {
            QApplication::connect(this, SIGNAL(roomctrl_to_events_rfid( QString , QString )),
                                  events, SLOT(from_roomctrl_rfid( QString , QString )));

            emit roomctrl_to_events_rfid ( rc_usr_list[i].rc_usr_id , rc_usr_list[i].rc_usr_user );

            QApplication::disconnect(this, SIGNAL(roomctrl_to_events_rfid( QString , QString )),
                                  events, SLOT(from_roomctrl_rfid( QString , QString )));

            rc_reg_list[actual_reg].rc_reg_id = rc_usr_list[i].rc_usr_id;
            rc_reg_list[actual_reg].rc_reg_user = rc_usr_list[i].rc_usr_user;

            autorizate = 1;

            QApplication::connect(this, SIGNAL(validate_rfid( const QByteArray &)), port, SLOT(send( const QByteArray &)));

            emit validate_rfid( "#1$" );

            QApplication::disconnect(this, SIGNAL(validate_rfid( const QByteArray & ) ), port, SLOT(send(const QByteArray &)));
        }
    }

    if ( autorizate == 0 )
    {
        QApplication::connect(this, SIGNAL(roomctrl_to_events_rfid( QString , QString )),
                              events, SLOT(from_roomctrl_rfid( QString , QString )));

        emit roomctrl_to_events_rfid ( "ACCESS DENIED" , "UNREGISTERED ID" );

        QApplication::disconnect(this, SIGNAL(roomctrl_to_events_rfid( QString , QString )),
                              events, SLOT(from_roomctrl_rfid( QString , QString )));

        rc_reg_list[actual_reg].rc_reg_id = id;
        rc_reg_list[actual_reg].rc_reg_user = "INVALID";

        QApplication::connect(this, SIGNAL(validate_rfid( const QByteArray &)), port, SLOT(send( const QByteArray &)));

        emit validate_rfid( "#0$" );

        QApplication::disconnect(this, SIGNAL(validate_rfid( const QByteArray &)), 0, 0);
    }

    ui->Registers->clearContents();

    while ( ui->Registers->rowCount() > 0 )
        ui->Registers->removeRow(0);

    for( int j = actual_reg ; j > registros_iniciales - 1 ; j-- )
    {
        ui->Registers->insertRow(ui->Registers->rowCount());

        ui->Registers->setItem(ui->Registers->rowCount()-1, 0, new QTableWidgetItem(rc_reg_list[j].rc_reg_date));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 1, new QTableWidgetItem(rc_reg_list[j].rc_reg_time));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 2, new QTableWidgetItem(rc_reg_list[j].rc_reg_id));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 3, new QTableWidgetItem(rc_reg_list[j].rc_reg_user));
        ui->Registers->setItem(ui->Registers->rowCount()-1, 4, new QTableWidgetItem(rc_reg_list[j].rc_reg_door));
    }
}

// ***************************************************************************************************************************
// SLOT*FROM*PORT*RFID********************************************************************************************************
// ***************************************************************************************************************************

/**
   \fn void from_port_rfid( QByteArray aux )
   \brief Slot de recepción del rfid.
   \details Recibe una string con la id de la tarjeta leída. Comprueba si es un usuario válido y devuelve un 1 si lo es a través del puerto.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
    \param aux : Tipo QByteArray, valor de id recibido de la placa.
   \return void
*/

void RoomCTRL::from_port_rfid_to_users ( QByteArray id )
{
    QApplication::connect(this, SIGNAL(roomctrl_to_users_new_id ( QByteArray )),
                          ( Users *) users_p, SLOT( from_roomctrl_new_id ( QByteArray )));

    emit roomctrl_to_users_new_id ( id );
}
