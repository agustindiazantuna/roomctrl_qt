#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QObject>
#include <QStringList>
#include "port.h"
#include "connection.h"
#include "roomctrl.h"

#include <iostream>

using namespace std;

namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:

    explicit Login(QWidget *parent = 0);

    QList<RoomCTRL::pc_usr_nodo> pc_usr_list_login;

    RoomCTRL *roomctrl;

    ~Login();

    bool Connected(void);

    void setPortConnected( bool state );

    void ProcessCommand (const QByteArray & received_data);

    QByteArray received_data;

    bool user_logged;

protected:

    int cant_received_bytes;

    int cant_send_bytes;

    char state;

    Port *port;

    QString *command;

    Connection *connection;

private slots:

    void on_B_Connect_clicked();

    void on_B_Log_clicked();

    void on_Close_clicked();

    void on_LE_Password_returnPressed();

    void onReceivedData(void);

    void onConnected(bool connected);

    void onConnectClicked(QString portName);

private:

    Ui::Login *ui;

signals:

    void login_to_roomctrl ( QList<RoomCTRL::pc_usr_nodo> , int , RoomCTRL * , Port * , int * );

    void port_hum ( QByteArray );

    void port_tem ( QByteArray );

    void port_cam ( QByteArray );

    void port_door ( QByteArray );

    void port_rfid ( QByteArray );

    void port_rfid_to_users ( QByteArray );

};

#endif // LOGIN_H
