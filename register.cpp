/**
   \file register.cpp
   \brief Contiene todos los metodos de la clase Register.
   \details Muestra los registros disponibles.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "register.h"
#include "ui_register.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Register( QWidget *parent )
    \brief Constructor de la clase Register.
    \details Abre la ventana de registros.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Register::Register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Register)
{
    close_b = false;

    ui->setupUi(this);

    ui->Room_Reg->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Room_User_Reg->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Ctrl_Reg->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->Ctrl_User_Reg->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Register( )
    \brief Destructor de la clase Register.
    \details Cierra la ventana de registros.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Register::~Register()
{
    on_B_Close_clicked( );

    delete ui;
}

// ***************************************************************************************************************************
// HANDLER*CLOSE*EVENT********************************************************************************************************
// ***************************************************************************************************************************

/**
   \fn void closeEvent ( QCloseEvent *event )
   \brief Handler del botón close de la ventana (x).
   \details Función perteneciente a la clase QWidget, llama al slot del botón close cuando se cierra la ventana.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
   \param event : Tipo QCloseEvent *, contiene parámetros que describen el tipo de evento.
   \return void
*/

void Register::closeEvent ( QCloseEvent *event )
{
    if ( !close_b )
        on_B_Close_clicked();

    QWidget::closeEvent(event);
}


// ***************************************************************************************************************************
// CELL*CLICKLED**************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_Room_Reg_cellClicked ( int row , int column)
    \brief Slot de celda clickeada de la tabla de registros de roomctrl.
    \details Recive el número de fila y columna, obtiene la string de ese campo, abre la imagen y la muestra usando OpenCV.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param row : Tipo int, número de fila.
    \param column : Tipo int, número de columna.
    \return void
*/

void Register::on_Room_Reg_cellClicked(int row, int column)
{
    if ( column == 4 || column == 5 )
    {
        QString name_img;

        name_img =  ui->Room_Reg->item( row , column )->text().toLatin1();

        QDir directory(".");                                                /// Abrimos pc_usr
        QString link = directory.filePath("img/"+name_img);

        IplImage *img;

        img = cvLoadImage( link.toLatin1() );

        cvNamedWindow ( name_img.toLatin1() , CV_WINDOW_AUTOSIZE );

        cvMoveWindow ( name_img.toLatin1() , 100 , 100 );

        cvShowImage( name_img.toLatin1() , img );

        cvReleaseImage( & img );
    }
}

// ***************************************************************************************************************************
// BUTTON*CLOSE***************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Close_clicked( )
    \brief Slot del botón Close al estar clickeado.
    \details Comunica esta ventana con RoomCTRL devolviendo las listas.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Register::on_B_Close_clicked()
{
    close_b = true;

    close();
}

// ***************************************************************************************************************************
// SLOT*FROM*ROOMCTRL*********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void from_roomctrl( QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux, QList<RoomCTRL::rc_reg_nodo> rc_reg_list_aux, QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux )
    \brief Slot de recepción de datos de la ventana RoomCTRL.
    \details Recibe las listas, las guarda en variables miembro y las muestras en la correspondiente QTable.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param pc_reg_list_aux : Tipo QList<RoomCTRL::pc_reg_nodo>, lista de registros de actividad en la PC.
    \param rc_reg_list_aux : Tipo QList<RoomCTRL::rc_reg_nodo>, lista de registros de actividad en la placa.
    \param pc_usr_list_aux : Tipo QList<RoomCTRL::pc_usr_nodo>, lista de usuarios de la PC.
    \param rc_usr_list_aux : Tipo QList<RoomCTRL::rc_usr_nodo>, lista de usuarios de la placa.
    \return void
*/

void Register::from_roomctrl(QList<RoomCTRL::pc_reg_nodo> pc_reg_list_aux , QList<RoomCTRL::rc_reg_nodo> rc_reg_list_aux ,
                             QList<RoomCTRL::pc_usr_nodo> pc_usr_list_aux , QList<RoomCTRL::rc_usr_nodo> rc_usr_list_aux )
{
    pc_reg_list_register = pc_reg_list_aux;
    rc_reg_list_register = rc_reg_list_aux;
    pc_usr_list_register = pc_usr_list_aux;
    rc_usr_list_register = rc_usr_list_aux;

    // PESTAÑA ROOM

        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);             // A partir de Qt V5.0
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
        ui->Room_Reg->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);

    for ( int i = 0 ; i < rc_reg_list_register.size() ; i++ )
    {
        ui->Room_Reg->insertRow(ui->Room_Reg->rowCount());

        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 0 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_date));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 1 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_time));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 2 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_id));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 3 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_user));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 4 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_img1));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 5 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_img2));
        ui->Room_Reg->setItem(ui->Room_Reg->rowCount()-1, 6 , new QTableWidgetItem(rc_reg_list_register[i].rc_reg_door));
    }

    // PESTAÑA CTRL

        ui->Ctrl_Reg->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);             // A partir de Qt V5.0
        ui->Ctrl_Reg->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        ui->Ctrl_Reg->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
        ui->Ctrl_Reg->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

    for ( int i = 0 ; i < pc_reg_list_register.size() ; i++ )
    {
        ui->Ctrl_Reg->insertRow(ui->Ctrl_Reg->rowCount());

        ui->Ctrl_Reg->setItem(ui->Ctrl_Reg->rowCount()-1, 0 , new QTableWidgetItem(pc_reg_list_register[i].pc_reg_date));
        ui->Ctrl_Reg->setItem(ui->Ctrl_Reg->rowCount()-1, 1 , new QTableWidgetItem(pc_reg_list_register[i].pc_reg_time));
        ui->Ctrl_Reg->setItem(ui->Ctrl_Reg->rowCount()-1, 2 , new QTableWidgetItem(pc_reg_list_register[i].pc_reg_user));
        ui->Ctrl_Reg->setItem(ui->Ctrl_Reg->rowCount()-1, 3 , new QTableWidgetItem(pc_reg_list_register[i].pc_reg_action));
    }

    // PESTAÑA ROOM USER

        ui->Room_User_Reg->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);                 // A partir de Qt V5.0
        ui->Room_User_Reg->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    for ( int i = 0 ; i < rc_usr_list_register.size() ; i++ )
    {
        ui->Room_User_Reg->insertRow(ui->Room_User_Reg->rowCount());

        ui->Room_User_Reg->setItem(ui->Room_User_Reg->rowCount()-1, 0 , new QTableWidgetItem(rc_usr_list_register[i].rc_usr_id));
        ui->Room_User_Reg->setItem(ui->Room_User_Reg->rowCount()-1, 1 , new QTableWidgetItem(rc_usr_list_register[i].rc_usr_user));
    }

    // PESTAÑA CTRL USER

        ui->Ctrl_User_Reg->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);                 // A partir de Qt V5.0

    for ( int i = 0 ; i < pc_usr_list_register.size() ; i++ )
    {
        ui->Ctrl_User_Reg->insertRow(ui->Ctrl_User_Reg->rowCount());

        ui->Ctrl_User_Reg->setItem(ui->Ctrl_User_Reg->rowCount()-1, 0 , new QTableWidgetItem(pc_usr_list_register[i].pc_usr_user));
    }
}
