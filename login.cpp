/**
   \file login.cpp
   \brief Contiene todos los metodos de la clase Users.
   \details Administra los usuarios de RoomCTRL.
   \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
   \date 2014.11.01
*/

// ***************************************************************************************************************************
// INCLUDES*******************************************************************************************************************
// ***************************************************************************************************************************

#include "login.h"
#include "ui_login.h"

// ***************************************************************************************************************************
// CONSTRUCTOR****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn Login( QWidget *parent )
    \brief Constructor de la clase Login.
    \details Abre la ventana de login, carga la lista de usuarios de la PC, si no existe la crea con el usuario administrador.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Login::Login(QWidget *parent) :
    QDialog(parent), cant_received_bytes(0), cant_send_bytes(0), port(new Port(this)),
    ui(new Ui::Login)
{
    user_logged = false;

    ui->setupUi(this);

    ui->LE_Password->setEchoMode(QLineEdit::Password);  // Password se vea oculta con *****

    setPortConnected(false);

    QObject::connect(port, SIGNAL(connected(bool)), this, SLOT(onConnected(bool)));

    QDir directory(".");

    directory.mkdir ( "img" );  // Crea las carpetas de imagenes y registros, en caso de no existir.
    directory.mkdir ( "reg" );

    QString path = directory.filePath("reg/pc_usr.txt");   // Abrimos pc_usr.
    QFile file_pc_usr ( path );

    if( file_pc_usr.open(QIODevice::ReadOnly) ) // Si existe archivo llena lista de usuarios de la pc.
    {
        while ( !file_pc_usr.atEnd() )
        {
            QString data = file_pc_usr.readLine(300);
            data.remove( QRegExp("\n") );
            QStringList qsl_aux = data.split(",");

            RoomCTRL::pc_usr_nodo aux;
            aux.pc_usr_user = qsl_aux[0];
            aux.pc_usr_password = qsl_aux[1];

            pc_usr_list_login.append(aux);
        }
        file_pc_usr.close();
    }

    if ( pc_usr_list_login.isEmpty() ) // Si la lista está vacía (no existe archivo) agrego al primer usuario admin con password admin
    {
        RoomCTRL::pc_usr_nodo aux;
        aux.pc_usr_user = "admin";
        aux.pc_usr_password = "admin";

        pc_usr_list_login.append(aux);
    }
}

// ***************************************************************************************************************************
// DESTRUCTOR*****************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn ~Login( )
    \brief Destructor de la clase Login.
    \details Cierra la ventana de login.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

Login::~Login()
{
    on_Close_clicked();

    delete ui;
}

// ***************************************************************************************************************************
// BUTTON*CONNECT*************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Connect_clicked( )
    \brief Slot del botón Connect al ser clickeado.
    \details Crea el objeto coneccion y abre la ventana del mismo. Conecta el puerto con connection y connection con este objeto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Login::on_B_Connect_clicked()
{
    connection = new Connection(port->isConnected(), this);

    QObject::connect(connection, SIGNAL(connectClicked(QString)), this, SLOT(onConnectClicked(QString)));

    QObject::connect(port, SIGNAL(connected(bool)), connection, SLOT(UpdateConnectionState(bool)));

    connection->show();
}

// ***************************************************************************************************************************
// BUTTON*LOGIN***************************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_B_Log_clicked( )
    \brief Slot del botón Login al ser clickeado.
    \details Comprueba que el usuario y la contraseña sean correctas, en caso de que lo sean abre la ventan RoomCTRL y se cierra a si misma.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Login::on_B_Log_clicked()
{
    RoomCTRL::pc_usr_nodo aux;

    aux.pc_usr_user = ui->LE_User->displayText().toLatin1();
    aux.pc_usr_password = ui->LE_Password->text();

    int autorizate = 0;

    for ( int i = 0 ; i < pc_usr_list_login.size() ; i++ )              // Comprueba si user/pass están en la lista
    {
        if ( pc_usr_list_login[i].pc_usr_user == aux.pc_usr_user )
          {
                if  ( pc_usr_list_login[i].pc_usr_password == aux.pc_usr_password )
                {
                    autorizate = 1;

                    roomctrl = new RoomCTRL;
                    roomctrl->showMaximized();

                    QApplication::connect(this, SIGNAL(login_to_roomctrl(QList<RoomCTRL::pc_usr_nodo>, int, RoomCTRL * , Port * , int * )),
                                          roomctrl, SLOT(from_login(QList<RoomCTRL::pc_usr_nodo>, int, RoomCTRL * , Port * , int * )));

                    emit login_to_roomctrl ( pc_usr_list_login , i , roomctrl , port , (int*) this );

                    user_logged = true;

                    ui->LE_User->clear();
                    ui->LE_Password->clear();

                    close();
                }
          }
    }

    if ( autorizate == 0 )
    {
        QMessageBox msgBox;
        msgBox.setText("INVALID USER OR PASSWORD");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}

// ***************************************************************************************************************************
// BUTTON*CLOSE***************************************************************************************************************
// ***************************************************************************************************************************
/**
    \fn void on_Close_clicked( )
    \brief Slot del botón Close al ser clickeado.
    \details Cierra la ventana.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Login::on_Close_clicked()
{
    close();
}

// ***************************************************************************************************************************
// ENTER*COMO*CLICK***********************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void on_Close_clicked( )
    \brief Slot del LineEdit Password.
    \details Al presionar "enter" se llama al slot de clickear el botón Login.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Login::on_LE_Password_returnPressed()
{
     on_B_Log_clicked();
}

// ***************************************************************************************************************************
// SLOT*CONNECTCLICKED*PORT***************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void onConnectClicked( QString physName )
    \brief Slot del comunicación con el objeto Port.
    \details Conecta el puerto, si fue clickeado el botón connect en la ventana Port.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param physName : Tipo QString, nombre del puerto.
    \return void
*/

void Login::onConnectClicked( QString physName )
{
    if (port != NULL)
    {
        if (!(port->isConnected()))
        {
#ifdef Q_OS_WIN
            port->connect(portName);
#elif (defined Q_OS_UNIX)
            port->connect(physName);
#endif
        }
        else
            port->close();
    }

//   qDebug() << "APP ]]" << (port->isConnected()?"conectado":"desconectado");
}

// ***************************************************************************************************************************
// SLOT*ONCONNECTED*PORT******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void onConnected( bool connected )
    \brief Slot de recepción de datos del Port.
    \details Conecta o desconecta la señal de lectura del puerto en base a su estado, con la slot de onReceivedData.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param connected : Tipo bool, estado de la conexión.
    \return void
*/

void Login::onConnected(bool connected)
{
    if(connected)
    {
        setPortConnected(true);
        QObject::connect(port, SIGNAL(dataRead()), this, SLOT(onReceivedData()));
    }
    else
    {
        setPortConnected(false);
        QObject::disconnect(port, SIGNAL(dataRead()), this, SLOT(onReceivedData()));
    }
}

// ***************************************************************************************************************************
// SLOT*ONCONNECTED*PORT******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void onReceivedData( )
    \brief Slot de recepción de datos.
    \details Recibe los datos del puerto y llama a la función ProcessCommand(), al llegar la señal dataRead.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param void
    \return void
*/

void Login::onReceivedData()
{
    received_data = port->read();

//    cout<<"port->read() devolvio: "<<received_data.at(0)<<endl;
//    cout<<"Received data vale:"<<received_data.constData()<<endl;
//    cout<<"Received data size vale:"<<received_data.size()<<endl;

    cant_received_bytes += received_data.size();

    if( cant_received_bytes > 1)
        ProcessCommand(received_data);

    received_data.clear();
}

// ***************************************************************************************************************************
// SLOT*PROCCESSCOMMAND*******************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void ProcessCommand( const QByteArray & received_data )
    \brief Slot encargado de procesar los datos recibidos por el puerto.
    \details Analiza la trama, en base a la misma, conecta objetos y emite señales.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param received_data : Puntero tipo const QByteArray &, datos recibidos.
    \return void
*/

void Login::ProcessCommand (const QByteArray & received_data)
{
    if ( user_logged )
    {
        if( received_data.contains('$') && received_data.contains('#') )
        {
            QByteArray aux;
            int i = 0;

            while ( received_data[i] != '$' )
                i++;

            if ( received_data[i+1] == 't' )    // TEMPERATURA
            {
                aux[0] = received_data[i+2];
                aux[1] = received_data[i+3];

                if ( received_data[i+4] == '#' )
                {
                    QObject::connect(this , SIGNAL ( port_tem(QByteArray) ) , roomctrl , SLOT ( from_port_tem(QByteArray) ) );

                    emit ( port_tem(aux) );

                    QObject::disconnect(this, SIGNAL( port_tem ( QByteArray )), 0, 0);
                }
            }

            if ( received_data[i+1] == 'h' )    // HUMEDAD
            {
                aux[0] = received_data[i+2];
                aux[1] = received_data[i+3];

                QObject::connect(this , SIGNAL ( port_hum(QByteArray) ) , roomctrl , SLOT ( from_port_hum ( QByteArray) ) );

                emit ( port_hum ( aux ) );
            }

            if ( received_data[i+1] == 'c' )    // CÁMARA
            {
                aux[0] = received_data[i+2];

                QObject::connect(this , SIGNAL ( port_cam(QByteArray) ) , roomctrl , SLOT ( from_port_cam ( QByteArray) ) );

                if( aux.toDouble() == 1 )
                    roomctrl->cam_first = false;

                if( aux.toDouble() == 2 )
                    roomctrl->cam_second = false;

                emit ( port_cam ( aux ) );
            }

            if ( received_data[i+1] == 'd' )    // PUERTA
            {
                aux[0] = received_data[i+2];

                QObject::connect(this , SIGNAL ( port_door(QByteArray) ) , roomctrl , SLOT ( from_port_door(QByteArray) ) );

                emit ( port_door(aux) );
            }

            if ( received_data[i+1] == 'r' )    // RFID
            {
                for ( int j = 0 ; j <10 ; j++ )
                    aux[j] = received_data[i+2+j];

                QObject::connect(this , SIGNAL ( port_rfid(QByteArray) ) , roomctrl , SLOT ( from_port_rfid(QByteArray) ) );

                emit ( port_rfid (aux) );

                QObject::disconnect(this, SIGNAL (port_rfid(QByteArray)) , roomctrl , SLOT ( from_port_rfid(QByteArray) ) );
            }

            if ( received_data[i+1] == 'n' )    // RFID
            {
                for ( int j = 0 ; j <10 ; j++ )
                    aux[j] = received_data[i+2+j];

                QObject::connect(this , SIGNAL ( port_rfid_to_users(QByteArray) ) , roomctrl , SLOT ( from_port_rfid_to_users (QByteArray) ) );

                emit ( port_rfid_to_users (aux) );

                QObject::disconnect(this, SIGNAL ( port_rfid_to_users(QByteArray) ) , roomctrl , SLOT ( from_port_rfid_to_users (QByteArray) ) );
            }
        }
    }
}

// ***************************************************************************************************************************
// SLOT*SET*PORT*CONNECTED****************************************************************************************************
// ***************************************************************************************************************************

/**
    \fn void setPortConnected( bool state )
    \brief Slot encargado de habilitar los campos de Login.
    \details Habilita o deshabilita los campos en base al estado del puerto.
    \author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
    \date 2014.11.01
    \param state : Tipo bool, true si el puerto está conectado.
    \return void
*/

void Login::setPortConnected( bool state )
{
    if( state )
    {
        ui->B_Log->setEnabled(true);                    //Habilita el boton de logueo
        ui->LE_User->setEnabled(true);                  //Habilita el campo de usuario
        ui->LE_Password->setEnabled(true);              //Habilita el campo de constraseña
        ui->State->setText("Port: Connected");          //Avisa que puerto esta conectado
    }
    else
    {
        ui->B_Log->setDisabled(true);                   //Deshabilita el boton de logueo
        ui->LE_User->setDisabled(true);                 //Deshabilita el campo de usuario
        ui->LE_Password->setDisabled(true);             //Deshabilita el campo de contraseña
        ui->State->setText("Port: Disconnected");       //Avisa que el puerto esta desconectado
    }
}
